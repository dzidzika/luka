<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Scandiweb Products</title>
<meta name="viewport" content="width=device-width, initial-scale=1">

<link href="/template/css/main.css" rel="stylesheet" type="text/css"/>

</head>

<body>

<div class="container">

<div class="ptitle">
<h1>Product List</h1>
<p>
	<a id="submit" href="/add_product">Add Product</a>
</p> 
</div>
<hr>


<?php

if(isset($_GET['delete_id']))
	{
		
		$stmt_delete = $DB_con->prepare('DELETE FROM products WHERE id =:uid');
		$stmt_delete->bindParam(':uid',$_GET['delete_id']);
		$stmt_delete->execute();
		
		header("Location: ../index.php");
    }
    
    $stmt = $DB_con->prepare("SELECT * FROM products ORDER by id DESC");
    $stmt->execute();

    while($product = $stmt->fetch(PDO::FETCH_ASSOC)) {
    ?>

        <div class="products">
            <span><?php echo $product['sku']; ?></span>
            <h2><?php echo $product['name']; ?></h2>
            <span><?php echo $product['price']; ?>$</span>
            <?php if($product['size'] > 0) { ?><span>Size: <?php echo $product['size']; ?> Mb</span><?php } ?>
            <?php if($product['weight'] > 0) { ?> <span>Weight: <?php echo $product['weight']; ?> KG</span> <?php } ?>
            <?php if($product['height'] > 0) { ?> <span>Dimension: <?php echo $product['height']; ?>x<?php echo $product['width']; ?>x<?php echo $product['length']; ?></span><?php } ?>
        
            <a class="btn-delete" href="?delete_id=<?php echo $product['id']; ?>" title="Click For Delete Product" onclick="return confirm('Are You Sure You Want To Delete Product?')">Delete Product</a>
        </div>

	<?php
    }
?>  




</div><!-- end container -->

</body>
</html>