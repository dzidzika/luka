<?php


	if(isset($_POST['btnsave']))
	{
		$sku = $_POST['sku'];
		$name = $_POST['name'];
		$price = $_POST['price'];
		$size = $_POST['size'];
		$height = $_POST['height'];
		$width = $_POST['width'];
		$length = $_POST['length'];
		$weight = $_POST['weight'];
		
		
		
		if(empty($sku)){
			$errMSG = "Please Enter Sku Code !";
		}
		
	$stmt = $DB_con->prepare("SELECT * FROM products WHERE sku = :sku") or die($DB_con->error());
    $stmt->bindParam(':sku', $sku);
    $stmt->execute();

    if($stmt->rowCount() > 0)
    {
        $errMSG = "This SKU Code Is Already In Database !";
	}
	
		// if no error occured, continue ....
		if(!isset($errMSG))
		{

			$stmt = $DB_con->prepare('INSERT INTO products(sku,name,price,size,height,width,length,weight) 
			VALUES(:sku, :name, :price, :size, :height, :width, :length, :weight)');
			$stmt->bindParam(':sku',$sku);
			$stmt->bindParam(':name',$name);
			$stmt->bindParam(':price',$price);
			$stmt->bindParam(':size',$size);
			$stmt->bindParam(':height',$height);
			$stmt->bindParam(':width',$width);
			$stmt->bindParam(':length',$length);
			$stmt->bindParam(':weight',$weight);
			
			if($stmt->execute())
			{
				$successMSG = "<p>Product Added Succesfully...</p>";
				header("refresh:2; /index.php");
			}
			else
			{
				$errMSG = "Something Wrong....";
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>Add Product</title>

<link href="/template/css/main.css" rel="stylesheet" type="text/css"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/template/js/switcher.js"></script>

<scirpt type="text/javascript">

</script>

    </head>
    <body>
       


<div class="container">


<?php
	if(isset($errMSG)){
			?>
            <div class="alert error">
            	<strong><?php echo $errMSG; ?></strong>
            </div>
            <?php
	}
	else if(isset($successMSG)){
		?>
        <div class="alert success">
              <strong><?php echo $successMSG; ?></strong>
        </div>
        <?php
	}
	?>   

<form method="post">

<div class="ptitle">
<h1>Product Add</h1>
 
<p>
	<button type="submit" name="btnsave" id="submit" value="Save"/>Save</button>
</p> 
</div>
<hr>
	
<div class="form">

    <p>
       <label for="sku">SKU:</label>
       <input class="input" type="text"required autocomplete="off" name="sku" id="sku" />
    </p>

	<p>
		<label for="name">Name:</label>
		<input class="input" type="text" required autocomplete="off" name="name" id="name" />
	</p>

	<p>
		<label for="price">Price:</label>
		<input class="input" type="number" name="price" step="0.001" id="price" />
	</p>


	<td>
      <label for="product">Select type of product: </label>
    </td>
    <td>
      <select id="product" class="product" name="product">
        <option value="none" default>Select Product From List</option>
        <option value="dvd">DVD-Disc</option>
		<option value="dimension">Dimensions</option>
		<option value="book">Book</option>
      </select>
    </td>

    <fieldset class="product-form dvd">
        <label for="dvd">Size: </label>
        <input class="dvd input" id="dvd" type="number" name="size" step="0.001" min="0" placeholder="Enter DVD size" />
        <p>DVD disc size in MB</p>
    </fieldset>

    <fieldset class="product-form dimension">
      <table>
        <tr>
          <td>
            <label for="height">Height: </label>
          </td>
          <td>
            <input class="dimension input" id="height" name="height" type="number" min="0" placeholder="Height" />
          </td>
        </tr>
        <tr>
          <td>
            <label for="width">Width: </label>
          </td>
          <td>
            <input class="dimension input" id="width" name="width" type="number" min="0" placeholder="Width" />
          </td>
        </tr>
        <tr>
          <td>
            <label for="length">Length: </label>
          </td>
          <td>
            <input class="dimension input" id="length" name="length" type="number" min="0" placeholder="Length" />
          </td>
        </tr>
      </table>
      <p>Please provide dimensions in HxWxL format</p>
    </fieldset>

	<fieldset class="product-form book">
        <label for="book">Weight: </label>
        <input class="book input" id="book" name="weight" type="number" step="0.001" min="0" placeholder="Enter Book Weight" />
        <p>Book Weight in KG</p>
    </fieldset>
             
    <div>    <!-- end form class -->       

</form> 


</div><!-- end container -->



</body>
</html>

