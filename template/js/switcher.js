$(function() {
  $('.product').change(function() {
    var option = $(this).find('option:selected');
    var value = '.' + option.val();
    
    $('.product-form').hide();
    $('.product-form input, .product-form select').removeAttr('required');
    
    $(value).show(500);
    $(value + ' input, ' + value + ' select').attr('required', true);
  });
  $('.product').change();
});